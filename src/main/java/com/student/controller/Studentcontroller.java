package com.student.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.sis.entity.Student;
import com.sis.repo.StudentRepo;

@RestController
@RequestMapping("/sis")
public class Studentcontroller {

	@Autowired
	StudentRepo studentRepo;
	
	@GetMapping("/studentinformation")
	public @ResponseBody Student setStudentDisabled(@RequestParam("student")
	Long studentId, Boolean status) {
		
	Student student = studentRepo.getById(studentId);
		//System.out.println("student Name	 : " + student.getName());
		if(student==null)
			return null;
		//expense.set(status);
		studentRepo.save(student);
		return student;
	}
	
	
	@GetMapping("/setprodutdeactive")
	public @ResponseBody Student setStudentDeactive(@RequestParam("student") 
	Long studentId, int active) {
		
	 Student student = studentRepo.getById(studentId);
		//System.out.println("student Name : " + student.getName());
		if(student==null)
			return null;
		studentRepo.save(student);
		return student;
	}	
	

}
