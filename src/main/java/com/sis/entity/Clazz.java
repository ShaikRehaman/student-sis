package com.sis.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Clazz {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "student_id")
	private List<Student> student;

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "student_id")
	private List<Teacher> teacher;

	private Long subjactId;
	private String section;
	private String period;
	private Long time;
	private String classTecherName;

	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private Classroom classroom;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getSubjactId() {
		return subjactId;
	}

	public void setSubjactId(Long subjactId) {
		this.subjactId = subjactId;
	}

	public String getSection() {
		return section;
	}

	public void setSection(String section) {
		this.section = section;
	}

	public String getPeriod() {
		return period;
	}

	public void setPeriod(String period) {
		this.period = period;
	}

	public Long getTime() {
		return time;
	}

	public void setTime(Long time) {
		this.time = time;
	}

	public String getClassTecherName() {
		return classTecherName;
	}

	public void setClassTecherName(String classTecherName) {
		this.classTecherName = classTecherName;
	}

	public Clazz() {
		super();
		// TODO Auto-generated constructor stub
	}

}
