package com.sis.entity;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Fees {
     @Id
     @GeneratedValue(strategy = GenerationType.AUTO)
	 private Long id;
     
    @ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    private Student student;
	
	private double admissionFees;
	private double examFees;
	private Long dueDate;
	private String status;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public double getAdmissionFees() {
		return admissionFees;
	}
	public void setAdmissionFees(double admissionFees) {
		this.admissionFees = admissionFees;
	}
	public double getExamFees() {
		return examFees;
	}
	public void setExamFees(double examFees) {
		this.examFees = examFees;
	}
	public long getDueDate() {
		return dueDate;
	}
	public void setDueDate(Long dueDate) {
		this.dueDate = dueDate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Fees() {
		super();
		// TODO Auto-generated constructor stub
	}

	
}