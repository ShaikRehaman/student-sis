package com.sis.entity;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Course {
	
	@Id
    @GeneratedValue(strategy =GenerationType.AUTO)
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
	private Student student;

    private String courseName;
    private double capacity;

    public Long getId() {
		return id;
	}
	 public void setId(Long id) {
		this.id = id;
	}

       public String getCourseName() {
	    return courseName;
    }
      public void setCourseName(String courseName) {
	    this.courseName = courseName;
    }
      public double getCapacity() {
	    return capacity;
    }
      public void setCapacity(double capacity) {
	   this.capacity = capacity;
    }
	public Course() {
		super();
		// TODO Auto-generated constructor stub
	}


    }
