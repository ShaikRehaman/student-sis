package com.sis.entity;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity
public class Admission {

    @Id
    @GeneratedValue(strategy =GenerationType.AUTO)
    private Long id ;
    
    @OneToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
   	@JoinColumn(name ="Admission_id",referencedColumnName = "id")
   	private Admission admission;
    
    private Long date;
    private String motherName;
    private String fatherName;
    private String permanentAddress;
    
    public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getDate() {
		return date;
	}
	public void setDate(Long date) {
		this.date = date;
	}
	
	

    public String getMotherName() {
		return motherName;
	}
	public void setMotherName(String motherName) {
		this.motherName = motherName;
	}
	public String getFatherName() {
		return fatherName;
	}
	public void setFatherName(String fatherName) {
		this.fatherName = fatherName;
	}
	public String getPermanentAddress() {
		return permanentAddress;
	}
	public void setPermanentAddress(String permanentAddress) {
		this.permanentAddress = permanentAddress;
	}
	public Admission() {
		super();
		// TODO Auto-generated constructor stub
	}
		
	
	

}
