package com.sis.entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Attendance {

      @Id
      @GeneratedValue(strategy =GenerationType.AUTO)
      private Long id ;
      
      @ManyToOne(fetch = FetchType.LAZY,cascade = CascadeType.ALL)
	  private Student student;
   
      private String studentName;
      private Long  date;
      private String status;
      private String parcentage;
      
	  public Long getId() {
		return id;
	}
	  public void setId(Long id) {
		this.id = id;
	}
	  public String getStudentName() {
		return studentName;
	}
	  public void setStudentName(String studentName) {
		this.studentName = studentName;
	}
	  public Long getDate() {
		return date;
	}
	  public void setDate(Long date) {
		this.date = date;
	}
	  public String getStatus() {
		return status;
	}
	 public void setStatus(String status) {
		this.status = status;
	}
	  public String getParcentage() {
		return parcentage;
	}
	  public void setParcentage(String parcentage) {
		this.parcentage = parcentage;
	}
	public Attendance() {
		super();
		// TODO Auto-generated constructor stub
	}
      
   
	}
	

