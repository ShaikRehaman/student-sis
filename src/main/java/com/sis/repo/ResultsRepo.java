package com.sis.repo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sis.entity.Results;
@Repository

public interface ResultsRepo  extends JpaRepository<Results, Long> {
}


