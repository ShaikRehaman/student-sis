package com.sis.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sis.entity.Admission;
@Repository
public interface AdmissionRepo extends JpaRepository<Admission, Long> {



}
