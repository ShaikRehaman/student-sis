package com.sis.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sis.entity.Student;

@Repository
public interface StudentRepo extends JpaRepository<Student, Long> {

}
