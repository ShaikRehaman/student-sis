package com.sis.repo;

	import org.springframework.data.jpa.repository.JpaRepository;
	import org.springframework.stereotype.Repository;

import com.sis.entity.Course;
	@Repository
	public interface CourseRepo extends JpaRepository<Course, Long> {

}
