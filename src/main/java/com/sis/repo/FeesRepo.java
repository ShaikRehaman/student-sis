package com.sis.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sis.entity.Fees;

@Repository
public interface FeesRepo extends JpaRepository<Fees, Long> {
	

}
