package com.sis.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sis.entity.Attendance;
@Repository
public interface AttendanceRepo extends JpaRepository<Attendance, Long> {

}
