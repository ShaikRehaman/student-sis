package com.sis.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sis.entity.Exam;

@Repository
public interface ExamRepo extends JpaRepository<Exam, Long> {

}
