package com.sis.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sis.entity.Clazz;

@Repository
public interface ClazzRepo extends JpaRepository<Clazz, Long> {
	

}
