package com.sis.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sis.entity.Classroom;

@Repository
public interface ClassroomRepo extends JpaRepository<Classroom, Long> {

}
