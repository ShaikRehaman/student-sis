package com.sis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

import springfox.documentation.swagger2.annotations.EnableSwagger2WebMvc;

@SpringBootApplication
@EnableSwagger2WebMvc

public class SisApplication extends SpringBootServletInitializer{

	public static void main(String[] args) {
		SpringApplication.run(SisApplication.class, args);
	}

}
